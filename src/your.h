#ifndef _YOUR_H_
#define _YOUR_H_

RcHitEnum MoveBomb(CharacterData *my);
RcHitEnum HitEnemyToBomb(CharacterData *my, CharacterData *your);
RcHitEnum MoveEnemyShot(CharacterData *my);
RcHitEnum MoveBound(CharacterData *my);
void CreateEnemy1(Sint16 x, Sint16 y);
void CreateEnemy2(Sint16 x, Sint16 y);
void CreateWall(Sint16 x);
RcHitEnum MoveWall(CharacterData *my);

#endif
