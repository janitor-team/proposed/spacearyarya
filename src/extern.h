#ifndef _EXTERN_H_
#define _EXTERN_H_

#include "misc.h"
#include "opening.h"
#include "game.h"
#include "load.h"
#include "my.h"
#include "your.h"
#include "boss.h"
#include "ranking.h"

#ifdef _EXTERN_DEF_
#define EXTERN 
#else
#define EXTERN extern
#endif

EXTERN CharacterData Cchr;          /**                **/
EXTERN RankingData **Ranking;
EXTERN StageData **StageDatas;      /** stage data     **/
EXTERN RootData *Root;              /** root data      **/
EXTERN PixData **PixMy;             /** my          **/
EXTERN PixData **PixShot;           /** my shot     **/
EXTERN PixData **PixBack;           /** back ground **/
EXTERN PixData **PixSky;            /** back ground **/
EXTERN PixData **PixEShot;          /** enemy shot  **/
EXTERN PixData **PixEnemy1;         /** enemy 1     **/
EXTERN PixData **PixEnemy2;         /** enemy 2     **/
EXTERN PixData **PixBomb;           /** bomb        **/
EXTERN PixData **PixKage;           /** kage        **/
EXTERN PixData **PixWall;           /** wall 1      **/
EXTERN PixData **PixWall2;          /** wall 2      **/
EXTERN PixData **PixBoss1;
EXTERN PixData **PixBoss2;
EXTERN PixData **PixBoss3;

#endif
